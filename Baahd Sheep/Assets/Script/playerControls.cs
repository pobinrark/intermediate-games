﻿using UnityEngine;
using System.Collections;

public class playerControls : MonoBehaviour {
    public float speed = 6.0F;
    
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;
    private Vector3 moveDirection = Vector3.zero;
    public Rigidbody rb;
    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
	}
    
    void Update()
    {
        CharacterController controller = GetComponent<CharacterController>();
        if (controller.isGrounded)
        {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, 0);
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= speed;
            if (Input.GetButton("Jump"))
            {
                moveDirection.y = jumpSpeed;

            }
        }
        if(!controller.isGrounded)
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                moveDirection.x -= speed * 1.5f;
            }
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                moveDirection.x += speed * 1.5f;
            }
        }
        
        moveDirection.y -= gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);
        
    }
}
